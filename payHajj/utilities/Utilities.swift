//
//  Utilities.swift
//
//  Created by MacBook Pro on 24/12/2016.
//  Copyright © 2016 MacBook Pro. All rights reserved.
//

import Foundation
import UIKit

class Utilities: NSObject {

    static func showAlert(_ viewController : UIViewController,messageToDisplay : String ) -> Void
    {
        

        
        let alertController = UIAlertController(title: "", message: messageToDisplay, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "تم", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        alertController.dismiss(animated: true, completion: nil)
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    static func showloginAlert(_ vc : UIViewController,messageToDisplay : String ) -> Void
    {
        
        
        let alertController = UIAlertController(title: "", message: messageToDisplay, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "تسجيل الدخول", style: .default, handler: { (action) in
            vc.performSegue(withIdentifier: "login", sender: nil)
        }))
        alertController.addAction(UIAlertAction(title: "إلغاء", style: .cancel, handler: nil))
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
   
    
}
