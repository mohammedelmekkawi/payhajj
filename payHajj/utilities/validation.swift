//
//  validation.swift
//
//  Created by Mohamed Elmakkawy on 7/31/17.
//  Copyright © 2017 Mohamed Elmakkawy. All rights reserved.
//

import Foundation
import UIKit

class validation {
   class func isValidEmail(_ Str:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: Str)
    }
    
   class func textFaildValidation(_ textFaild:UITextField)->Bool{
        if textFaild.text == "" {
            textFaild.layer.borderColor = UIColor.red.cgColor
            textFaild.layer.borderWidth = 1.0
            return true
        }else{
            textFaild.layer.borderColor = UIColor.clear.cgColor
            textFaild.layer.borderWidth = 1.0
            return false
        }
    }
    
   class func textFaildValidationColor(_ textFaild:UITextField){
        if textFaild.text == "" {
            textFaild.layer.borderColor = UIColor.red.cgColor
            textFaild.layer.borderWidth = 1.0
        }else{
            textFaild.layer.borderColor = UIColor.clear.cgColor
            textFaild.layer.borderWidth = 1.0
        }
    }
    
    
}
