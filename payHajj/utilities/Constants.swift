
import Foundation
import UIKit


struct Constants {
    
    fileprivate static let ScreenSize: CGRect = UIScreen.main.bounds
    static let ScreenWidth = ScreenSize.width
    static let ScreenHeight = ScreenSize.height
    static let DeviceType = UIDevice.current.modelName
    static let backgroundViewAlpha = CGFloat(0.50)
    
    struct messages {
        static let netFaild = "الاتصال قد يكون بطئ .. اذا فشل حاول مره اخري"
    }
    
    struct UserDefaultsConstants {
        static let Email = "email"
        static let UserData = "UserData"
        static let Password = "password"
        
    }
    
    struct Notification {
        static let isLogin = "isLogin"
        static let success = "success"
        
    }
    
    struct APIProvider {
        
        //User
        static let APIBaseURL = "http://"
        static let ImagesBaseURL = "http://"
        static let registerCode = APIBaseURL + "registerCode"

    }
    
    
}

var isIpad:Bool{
    let device = Constants.DeviceType
    if device == .iPad2 || device == .iPad3 || device == .iPad4 || device == .iPadAir || device == .iPadPro || device == .iPadAir2 || device == .iPadMini || device == .iPadMini2 || device == .iPadMini3 || device == .iPadMini4 {
        return true
    }
    return false
}

