//
//  RightMenuViewController.swift
//  AKSideMenuSimple
//
//  Created by Diogo Autilio on 6/7/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import UIKit

public class RightMenuVC: UIViewController {

    
    var titles = ["معلوماتي",
                  "طلباتي",
                  "الإشعارات",
                  "الرسائل",
                  "شارك التطبيق",
                  "عن التطبيق",
                  "الشروط والاحكام",
                  "تواصل معنا",
                  "تسجيل خروج"]
    
 
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var unreadeGroubalObj = 2
    
    override public func viewDidLoad() {
        super.viewDidLoad()
       userName.text = "Mr.Ahmed"

    }


    
}

extension RightMenuVC:UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection sectionIndex: Int) -> Int {
        return titles.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! menuCell
        if indexPath.row == 2 {
           
            if unreadeGroubalObj == 0 || unreadeGroubalObj == nil  {
                cell.baggingView.backgroundColor = UIColor.clear
                cell.baggingLb.text = ""
            }else {
                cell.baggingView.backgroundColor = UIColor.red
                cell.baggingLb.text = "\(unreadeGroubalObj)"
            }
           
        }else {
            cell.baggingView.backgroundColor = UIColor.clear
            cell.baggingLb.text = ""
        }
        cell.titleLb.text = titles[indexPath.row]
    
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
            
            print("go to first screen")
            
        case 1:
            
            print("go to second screen")

            
        case 2:
            
            print("go to therd screen")

            
        case 3:
            
            print("go to fourth screen")
            
        case 4:
            
            print("go to fourth screen")

        case 5:
            print("go to aboutUs")
            
        
        case 7:
            print("go to contactUs")
            
        case 8:
            UserModel.LogOut()
            
        default:
            break
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 46
    }
}
