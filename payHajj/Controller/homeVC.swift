//
//  homeVC.swift
//  payHajj
//
//  Created by Mohamed Elmakkawy on 8/2/18.
//  Copyright © 2018 Mohamed Elmakkawy. All rights reserved.
//

import UIKit
import AVFoundation
import QRCodeReader
import CCMPopup

var initValue :Double? {didSet{}}

class homeVC: baseVC , QRCodeReaderViewControllerDelegate{

    
    @IBOutlet weak var creditLb: UILabel!
    
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        initValue = 100
        creditLb.text = "\(initValue ?? 0.0)"
        NotificationCenter.default.addObserver(self, selector: #selector(homeVC.success(notification:)), name:NSNotification.Name(rawValue: Constants.Notification.success), object: nil)
    }
    @objc func success(notification:NSNotification){
       self.performSegue(withIdentifier: "success", sender: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "success" {
            let popupSegue = segue as! CCMPopupSegue
            popupSegue.destinationBounds = CGRect(x: 0, y: 0 , width: (UIScreen.main.bounds.width-40), height: (UIScreen.main.bounds.height/2))
            popupSegue.backgroundBlurRadius = 0.7
            popupSegue.backgroundViewAlpha = Constants.backgroundViewAlpha
            popupSegue.backgroundViewColor = Color.mainColor
            popupSegue.dismissableByTouchingBackground = true
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        creditLb.text = "\(initValue ?? 0.0)"
    }

    @IBAction func payAction(_ sender: UIButton) {
       self.performSegue(withIdentifier: "pay", sender: nil)
    }
    
    @IBAction func reciveAction(_ sender: UIButton) {
        //self.performSegue(withIdentifier: "recive", sender: nil)
        readerVC.delegate = self
        
        // Or by using the closure pattern
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
        
            if result != nil {
            initValue = initValue! +  Double(result!.value)!
            self.creditLb.text = "\(initValue ?? 0.0)"
            self.performSegue(withIdentifier: "success", sender: nil)
            
        }
        }
        
        // Presents the readerVC as modal form sheet
        readerVC.modalPresentationStyle = .formSheet
        present(readerVC, animated: true, completion: nil)
    }
    
    
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        
        dismiss(animated: true, completion: nil)
    }
    
    //This is an optional delegate method, that allows you to be notified when the user switches the cameraName
    //By pressing on the switch camera button
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        //        if let cameraName = newCaptureDevice.device.localizedName {
        //            print("Switching capturing to: \(cameraName)")
        //        }
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        
        dismiss(animated: true, completion: nil)
    }
}

