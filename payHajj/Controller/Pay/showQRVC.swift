//
//  showQRVC.swift
//  payHajj
//
//  Created by Mohamed Elmakkawy on 8/2/18.
//  Copyright © 2018 Mohamed Elmakkawy. All rights reserved.
// j

import UIKit
import QRCode
import CCMPopup
class showQRVC: baseVC {

    @IBOutlet weak var QRImge: UIImageView!
    var currentValue = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()

        initValue = initValue! - currentValue
        let url = URL(string: "\(currentValue)")!
        let qrCode = QRCode(url)
        QRImge.image = qrCode?.image
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.navigationController?.popToRootViewController(animated: true)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.Notification.success), object: nil)
            
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "success" {
            let popupSegue = segue as! CCMPopupSegue
            popupSegue.destinationBounds = CGRect(x: 0, y: 0 , width: (UIScreen.main.bounds.width-40), height: (UIScreen.main.bounds.height/2))
            popupSegue.backgroundBlurRadius = 0.7
            popupSegue.backgroundViewAlpha = Constants.backgroundViewAlpha
            popupSegue.backgroundViewColor = Color.mainColor
            popupSegue.dismissableByTouchingBackground = true
        }
    }

   

}
