//
//  amountVC.swift
//  payHajj
//
//  Created by Mohamed Elmakkawy on 8/2/18.
//  Copyright © 2018 Mohamed Elmakkawy. All rights reserved.
//

import UIKit

class amountVC: baseVC {

    @IBOutlet weak var amountTxt: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "next" {
            let vc = segue.destination as! showQRVC
            vc.currentValue = Double(amountTxt.text!)!
        }
    }
    @IBAction func nextAction(_ sender: UIButton) {
        if amountTxt.text == "" {
            Utilities.showAlert(self, messageToDisplay: "من فضلك ادخل الرقم المحول")
        }else {
            self.performSegue(withIdentifier: "next", sender: nil)
        }
    }
    

}
