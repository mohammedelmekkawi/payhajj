//
//  successVC.swift
//  payHajj
//
//  Created by Mohamed Elmakkawy on 8/2/18.
//  Copyright © 2018 Mohamed Elmakkawy. All rights reserved.
//

import UIKit

class successVC: baseVC {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func successAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
