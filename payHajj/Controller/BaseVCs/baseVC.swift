//
//  baseVc.swift
//  Nahed
//
//  Created by Mohamed Elmakkawy on 10/3/17.
//  Copyright © 2017 Mohamed Elmakkawy. All rights reserved.
//

import UIKit

class baseVC: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

}

extension baseVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

}

