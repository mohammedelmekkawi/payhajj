//
//  baseTVC.swift
//  Nahed
//
//  Created by Mohamed Elmakkawy on 10/6/17.
//  Copyright © 2017 Mohamed Elmakkawy. All rights reserved.
//

import UIKit

class baseTVC: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        self.tableView.tableFooterView = UIView()
    }
    
}

extension baseTVC {
   override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
   override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}
